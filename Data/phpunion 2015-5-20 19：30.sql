/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50612
Source Host           : localhost:3306
Source Database       : phpunion

Target Server Type    : MYSQL
Target Server Version : 50612
File Encoding         : 65001

Date: 2015-05-20 19:32:10
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for php_access
-- ----------------------------
DROP TABLE IF EXISTS `php_access`;
CREATE TABLE `php_access` (
  `rid` smallint(5) unsigned NOT NULL,
  `nid` smallint(5) unsigned NOT NULL,
  KEY `gid` (`rid`),
  KEY `nid` (`nid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='管理员权限分配表';

-- ----------------------------
-- Records of php_access
-- ----------------------------

-- ----------------------------
-- Table structure for php_addons
-- ----------------------------
DROP TABLE IF EXISTS `php_addons`;
CREATE TABLE `php_addons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL COMMENT '插件名或标识',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '中文名',
  `description` text COMMENT '插件描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `config` text COMMENT '配置',
  `author` varchar(40) DEFAULT '' COMMENT '作者',
  `version` varchar(20) DEFAULT '' COMMENT '版本号',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '安装时间',
  `has_adminlist` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否有后台列表',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=294 DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of php_addons
-- ----------------------------
INSERT INTO `php_addons` VALUES ('265', 'Backup', '数据备份', '数据备份还原插件', '1', 'a:0:{}', '后盾网向军', '1.0', '1417657359', '1');
INSERT INTO `php_addons` VALUES ('290', 'Comment', '评论', '网站评论插件', '1', 'a:1:{s:10:\"REPLY_TIME\";s:1:\"0\";}', '后盾网向军', '1.0', '1418731991', '1');
INSERT INTO `php_addons` VALUES ('292', 'Search', '前台搜索', '前台搜索', '1', 'a:0:{}', '后盾网向军', '1.0', '1418887777', '0');
INSERT INTO `php_addons` VALUES ('293', 'Sitemap', '网站地图', '网站地图', '1', 'a:0:{}', '后盾网向军', '1.0', '1418887782', '0');

-- ----------------------------
-- Table structure for php_cate
-- ----------------------------
DROP TABLE IF EXISTS `php_cate`;
CREATE TABLE `php_cate` (
  `cid` mediumint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '栏目ID',
  `pid` mediumint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父级ID',
  `catname` char(30) NOT NULL DEFAULT '' COMMENT '栏目名称',
  `catdir` varchar(255) DEFAULT NULL,
  `cat_keyworks` varchar(255) DEFAULT '' COMMENT '栏目关键字',
  `cat_description` varchar(255) DEFAULT '' COMMENT '栏目描述',
  `index_tpl` varchar(200) DEFAULT '' COMMENT '封面模板',
  `list_tpl` varchar(200) DEFAULT '' COMMENT '列表页模板',
  `arc_tpl` varchar(200) DEFAULT '' COMMENT '内容页模板',
  `cat_html_url` varchar(200) DEFAULT '' COMMENT '栏目页URL规则\n\n',
  `arc_html_url` varchar(200) DEFAULT '' COMMENT '内容页URL规则',
  `cattype` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 栏目,2 封面,3 外部链接,4 单文章',
  `arc_url_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '文章访问方式 1 静态访问 2 动态访问',
  `cat_url_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '栏目访问方式 1 静态访问 2 动态访问',
  `cat_redirecturl` varchar(100) NOT NULL DEFAULT '' COMMENT '跳转url',
  `catorder` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目排序',
  `cat_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'channel标签调用时是否显示',
  `cat_seo_title` char(100) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `cat_seo_description` varchar(255) NOT NULL DEFAULT '' COMMENT 'SEO描述',
  `addtime` int(10) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='栏目表';

-- ----------------------------
-- Records of php_cate
-- ----------------------------
INSERT INTO `php_cate` VALUES ('1', '0', '微电影', 'movie', '', '', 'article_index.html', 'article_list.html', 'article_default.html', '{catdir}/{page}.html', '{catdir}/{y}/{m}{d}/{aid}.html', '2', '2', '2', '', '0', '1', '', '', null);
INSERT INTO `php_cate` VALUES ('2', '0', '网络视频', 'work', '', '', 'article_index.html', 'article_list.html', 'article_default.html', '{catdir}/{page}.html', '{catdir}/{y}/{m}{d}/{aid}.html', '2', '2', '2', '', '0', '1', '', '', null);
INSERT INTO `php_cate` VALUES ('4', '1', '励志微电影', 'lizhi', '', '', 'article_index.html', 'article_list.html', 'article_default.html', '{catdir}/{page}.html', '{catdir}/{y}/{m}{d}/{aid}.html', '1', '2', '2', '', '0', '1', '', '', null);
INSERT INTO `php_cate` VALUES ('5', '2', '网络搞笑', 'wamgluo', '', '', 'article_index.html', 'article_list.html', 'article_default.html', '{catdir}/{page}.html', '{catdir}/{y}/{m}{d}/{aid}.html', '1', '2', '2', '', '0', '1', '', '', null);

-- ----------------------------
-- Table structure for php_cate_access
-- ----------------------------
DROP TABLE IF EXISTS `php_cate_access`;
CREATE TABLE `php_cate_access` (
  `rid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目cid',
  `mid` smallint(1) NOT NULL DEFAULT '0' COMMENT '模型mid',
  `content` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许访问 1 允许 0 不允许',
  `add` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许投稿(添加) 1允许 0 不允许',
  `edit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许更新 1允许 0 不允许',
  `del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许删除 1允许 0 不允许',
  `order` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许排序 1允许 0 不允许',
  `move` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许移动 1允许 0 不允许',
  `audit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '允许审核栏目文章 1 允许 0 不允许',
  `admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为管理员权限 1 管理员 2 前台用户'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='栏目权限表';

-- ----------------------------
-- Records of php_cate_access
-- ----------------------------

-- ----------------------------
-- Table structure for php_config
-- ----------------------------
DROP TABLE IF EXISTS `php_config`;
CREATE TABLE `php_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cid` int(8) NOT NULL COMMENT '配置组ID',
  `name` varchar(50) DEFAULT NULL COMMENT '配置项变量名',
  `value` text COMMENT '配置值',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `type` enum('select','radio','text','textarea','group') DEFAULT 'text' COMMENT '类型',
  `info` text COMMENT '参数',
  `message` varchar(255) DEFAULT NULL COMMENT '描述提示',
  `sort` int(8) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) DEFAULT '1' COMMENT '1,正常 0禁用',
  `system` tinyint(1) DEFAULT NULL COMMENT '1系统,0普通',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_config
-- ----------------------------
INSERT INTO `php_config` VALUES ('1', '1', 'WEBNAME', '「PHP联盟」', '网站名称', 'text', '', '请输入网站名称！', '0', '1', '1', '1431367267');
INSERT INTO `php_config` VALUES ('2', '1', 'DOMAIN', 'http://localhost/PHPUnion', '网站域名', 'text', '', '请输入本站网址，不要以 / 结尾', '0', '1', '1', '1431367325');
INSERT INTO `php_config` VALUES ('3', '1', 'UPLOAD', 'upload', '附件目录', 'text', '', '请输入上传的附件目录！', '0', '1', '1', '1431369313');
INSERT INTO `php_config` VALUES ('4', '1', 'SEO_TITLE', '致力开发开发专业视频管理系统,视频上传转码,视频云储存', 'SEO标题', 'text', '', '请输入副的SEO标题！', '0', '1', '1', '1432095422');
INSERT INTO `php_config` VALUES ('5', '1', 'WEB_OPEN', '1', '网站开关', 'radio', '1|开启,0|关闭', '请选择网站是否开启', '0', '1', '1', '1432095612');
INSERT INTO `php_config` VALUES ('6', '5', 'WEB_STYLE', 'Default', '模板设置', 'text', '', '选择模板的配置项！', '0', '1', '1', '1432095933');
INSERT INTO `php_config` VALUES ('7', '1', 'WEB_CLOSE_MESSAGE', '网站正在维护中，请稍后访问！感谢您的支持。', '网站提示', 'textarea', '', '请输入网站关闭提示信息！', '0', '1', '1', '1432096147');
INSERT INTO `php_config` VALUES ('8', '1', 'EMAIL', 'Name_Cyu@Foxmail.com', '邮箱联系', 'text', '', '请输入站长邮箱！', '0', '1', '1', '1432096798');
INSERT INTO `php_config` VALUES ('9', '1', 'ADDRESS', '贵州省安顺市西秀区南水二路', '联系地址', 'text', '', '请输入现实联系地址！', '0', '1', '1', '1432096880');
INSERT INTO `php_config` VALUES ('10', '1', 'MOBILE', '18788654245', '联系电话', 'text', '', '请输入联系电话号码！', '0', '1', '1', '1432096935');

-- ----------------------------
-- Table structure for php_config_group
-- ----------------------------
DROP TABLE IF EXISTS `php_config_group`;
CREATE TABLE `php_config_group` (
  `cid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '配置组ID',
  `cname` varchar(100) DEFAULT NULL COMMENT '配置组标识',
  `ctitle` varchar(100) DEFAULT NULL COMMENT '配置组名称',
  `csort` int(8) DEFAULT '0' COMMENT '排序',
  `isshow` tinyint(1) DEFAULT NULL COMMENT '1显示,0隐藏',
  `system` tinyint(1) DEFAULT '0' COMMENT '1系统组 0普通组',
  `select` tinyint(1) DEFAULT '0' COMMENT '标签默认选择',
  `addtime` int(11) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_config_group
-- ----------------------------
INSERT INTO `php_config_group` VALUES ('1', 'system', '基本设置', '0', '1', '1', '1', '1431367065');
INSERT INTO `php_config_group` VALUES ('2', 'Member', '会员中心', '0', '1', '1', '0', '1431367088');
INSERT INTO `php_config_group` VALUES ('3', 'Performance', '性能设置', '0', '1', '1', '0', '1431367128');
INSERT INTO `php_config_group` VALUES ('4', 'Email', '邮箱设置', '0', '1', '1', '0', '1431367151');
INSERT INTO `php_config_group` VALUES ('5', 'Theme', '模板管理', '0', '0', '1', '0', '1431367166');
INSERT INTO `php_config_group` VALUES ('6', 'Images', '图组设置', '0', '1', '0', '0', '1431369269');

-- ----------------------------
-- Table structure for php_content
-- ----------------------------
DROP TABLE IF EXISTS `php_content`;
CREATE TABLE `php_content` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '栏目cid',
  `uid` int(10) unsigned NOT NULL COMMENT '用户uid',
  `fid` int(5) DEFAULT NULL COMMENT '推荐位id',
  `title` char(100) NOT NULL DEFAULT '' COMMENT '标题',
  `tag` char(255) DEFAULT NULL,
  `new_window` tinyint(1) NOT NULL DEFAULT '0' COMMENT '新窗口打开 0否 , 1是',
  `seo_title` char(100) NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `thumb` varchar(255) NOT NULL DEFAULT '' COMMENT '缩略图',
  `video_path` char(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT '',
  `click` int(6) NOT NULL DEFAULT '0' COMMENT '点击数',
  `redirecturl` varchar(255) NOT NULL DEFAULT '' COMMENT '转向链接',
  `html_path` varchar(255) NOT NULL DEFAULT '' COMMENT '自定义生成的静态文件地址',
  `url_type` tinyint(80) NOT NULL DEFAULT '3' COMMENT '访问方式  1 静态访问  2 动态访问  3 继承栏目',
  `arc_sort` mediumint(6) NOT NULL DEFAULT '0' COMMENT '排序',
  `content_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态  0 未审核 1 已审核 2 草稿',
  `keywords` varchar(100) DEFAULT '' COMMENT '关键字',
  `updatetime` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addtime` int(10) NOT NULL DEFAULT '0' COMMENT '添加时间',
  PRIMARY KEY (`aid`),
  KEY `uid` (`uid`),
  KEY `cid` (`cid`),
  KEY `content_status` (`content_status`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of php_content
-- ----------------------------
INSERT INTO `php_content` VALUES ('4', '4', '3', null, 'ddd', '', '1', '', '', null, '', '0', '', '', '3', '0', '1', '', '1431628058', '1431628058');

-- ----------------------------
-- Table structure for php_flag
-- ----------------------------
DROP TABLE IF EXISTS `php_flag`;
CREATE TABLE `php_flag` (
  `fid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) DEFAULT NULL,
  `addtime` int(10) DEFAULT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of php_flag
-- ----------------------------
INSERT INTO `php_flag` VALUES ('1', '热门', null);
INSERT INTO `php_flag` VALUES ('2', '置顶', null);
INSERT INTO `php_flag` VALUES ('3', '推荐', null);
INSERT INTO `php_flag` VALUES ('4', '图片', null);
INSERT INTO `php_flag` VALUES ('5', '精华', null);
INSERT INTO `php_flag` VALUES ('6', '幻灯片', null);
INSERT INTO `php_flag` VALUES ('7', '站长推荐', null);

-- ----------------------------
-- Table structure for php_hooks
-- ----------------------------
DROP TABLE IF EXISTS `php_hooks`;
CREATE TABLE `php_hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(40) NOT NULL DEFAULT '' COMMENT '钩子名称',
  `description` text NOT NULL COMMENT '描述',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `addons` varchar(255) NOT NULL DEFAULT '' COMMENT '钩子挂载的插件 ''，''分割',
  `status` tinyint(1) DEFAULT NULL COMMENT '状态',
  `is_system` tinyint(1) DEFAULT '0' COMMENT '系统钩子',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='钩子表';

-- ----------------------------
-- Records of php_hooks
-- ----------------------------
INSERT INTO `php_hooks` VALUES ('1', 'APP_BEGIN', '应用开始', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('2', 'PAGE_HEADER', '页面头部钩子，一般用于加载插件JS文件和JS代码', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('3', 'PAGE_FOOTER', '页面底部r钩子，一般用于加载插件CSS文件和代码', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('4', 'CONTENT_EDIT_BEGIN', '内容编辑前', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('5', 'CONTENT_EDIT_END', '内容编辑后', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('6', 'CONTENT_DEL', '内容删除后', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('7', 'CONTENT_ADD_BEGIN', '内容添加前', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('8', 'CONTENT_ADD_END', '内容添加后', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('10', 'ADMIN_LOGIN_START', '后台登录开始', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('11', 'ADMIN_LOGIN_SUCCESS', '后台登录成功', '1', '0', '', '1', '1');
INSERT INTO `php_hooks` VALUES ('9', 'CONTENT_SHOW', '内容显示', '1', '0', '', '1', '1');

-- ----------------------------
-- Table structure for php_node
-- ----------------------------
DROP TABLE IF EXISTS `php_node`;
CREATE TABLE `php_node` (
  `nid` smallint(6) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `title` char(80) NOT NULL DEFAULT '' COMMENT '中文标题',
  `group` char(100) NOT NULL DEFAULT '',
  `module` char(100) NOT NULL DEFAULT '' COMMENT '应用',
  `controller` char(50) NOT NULL DEFAULT '' COMMENT '控制器',
  `action` char(50) NOT NULL DEFAULT '' COMMENT '方法',
  `param` char(100) NOT NULL DEFAULT '' COMMENT '参数',
  `comment` varchar(255) NOT NULL DEFAULT '' COMMENT '备注',
  `is_show` tinyint(4) DEFAULT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '2' COMMENT '类型 1 权限控制菜单   2 普通菜单 ',
  `pid` mediumint(6) NOT NULL DEFAULT '0',
  `list_order` mediumint(6) NOT NULL DEFAULT '0' COMMENT '排序',
  `is_system` tinyint(1) NOT NULL DEFAULT '0' COMMENT '系统菜单 1 是  0 不是',
  `favorite` tinyint(1) NOT NULL DEFAULT '0' COMMENT '后台常用菜单   1 是  0 不是',
  `ico` char(50) DEFAULT '',
  PRIMARY KEY (`nid`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='节点表（后台菜单也使用）';

-- ----------------------------
-- Records of php_node
-- ----------------------------
INSERT INTO `php_node` VALUES ('1', '首页', '', 'Admin', 'Index', 'index', '', '', '1', '2', '0', '0', '0', '0', 'icon-home');
INSERT INTO `php_node` VALUES ('2', '内容管理', '', 'Admin', 'Content', 'index', '', '', '1', '2', '0', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('3', '栏目管理', '', 'Admin', 'Category', 'index', '', '', '1', '2', '0', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('4', '用户管理', '', 'Admin', 'User', 'index', '', '', '1', '2', '0', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('5', '权限管理', '', 'Admin', 'Admin', 'index', '', '', '1', '2', '0', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('6', '模板管理', '', 'Admin', 'Theme', 'index', '', '', '1', '2', '0', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('7', '内容模型', '', 'Admin', 'Model', 'index', '', '', '1', '2', '0', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('8', '扩展管理', '', 'Admin', 'Plug', 'index', '', '', '1', '2', '0', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('9', '系统设置', '', 'Admin', 'Config', 'webConfig', '', '', '1', '1', '1', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('10', '配置组', '', 'Admin', 'ConfigGroup', 'index', '', '', '1', '1', '1', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('11', '附件管理', '', 'Admin', '#', '#', '', '', '1', '1', '1', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('12', '路由管理', '', 'Admin', '#', '#', '', '', '1', '1', '1', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('13', '内容管理', '', 'Admin', 'Content', 'index', '', '', '1', '1', '2', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('14', '推 荐 位', '', 'Admin', '#', '#', '#', '', '1', '1', '2', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('16', '栏目管理', '', 'Admin', 'Category', 'index', '', '', '1', '1', '3', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('17', '栏目添加', '', 'Admin', 'Category', 'add', '', '', '1', '1', '3', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('18', '栏目修改', '', 'Admin', 'Category', 'edit', '', '', '0', '1', '3', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('19', '栏目删除', '', 'Admin', 'Category', 'del', '', '', '0', '1', '3', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('20', '用户管理', '', 'Admin', 'User', 'index', '', '', '1', '1', '4', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('21', '用户等级', '', 'Admin', 'Group', 'index', '', '', '1', '1', '4', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('22', '添加用户', '', 'Admin', 'User', 'add', '', '', '1', '1', '4', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('23', '修改用户', '', 'Admin', 'User', 'edit', '', '', '0', '1', '4', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('24', '删除用户', '', 'Admin', 'User', 'del', '', '', '0', '1', '4', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('25', '管理员组', '', 'Admin', 'Admin', 'index', '', '', '1', '1', '5', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('26', '管理组等级', '', 'Admin', 'Role', 'index', '', '', '1', '1', '5', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('27', '权限节点', '', 'Admin', 'Node', 'index', '', '', '1', '1', '5', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('28', '模板管理', '', 'Admin', 'Theme', 'index', '', '', '1', '1', '6', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('29', '模型管理', '', 'Admin', 'Model', 'index', '', '', '1', '1', '7', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('30', '创建模型', '', 'Admin', 'Model', 'add', '', '', '1', '1', '7', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('31', '插件管理', '', 'Admin', 'Plug', 'index', '', '', '1', '1', '8', '0', '0', '0', null);
INSERT INTO `php_node` VALUES ('32', '钩子管理', '', 'Admin', 'Hooks', 'index', '', '', '1', '1', '8', '0', '0', '0', null);

-- ----------------------------
-- Table structure for php_role
-- ----------------------------
DROP TABLE IF EXISTS `php_role`;
CREATE TABLE `php_role` (
  `rid` smallint(5) NOT NULL AUTO_INCREMENT,
  `rname` char(60) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '描述',
  `admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT '管理组 1 是 0 不是',
  `system` tinyint(1) NOT NULL DEFAULT '0' COMMENT '系统角色',
  `creditslower` mediumint(9) NOT NULL DEFAULT '0' COMMENT '积分<=时为此会员组',
  `comment_state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '评论不需要审核  1 不需要  2 需要',
  `allowsendmessage` tinyint(1) NOT NULL DEFAULT '1' COMMENT '允许发短消息  1 允许  2 不允许',
  PRIMARY KEY (`rid`),
  KEY `gid` (`rid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of php_role
-- ----------------------------
INSERT INTO `php_role` VALUES ('1', '系统管理员', '', '1', '1', '0', '1', '1');
INSERT INTO `php_role` VALUES ('2', '超级管理员', '', '1', '1', '0', '1', '1');
INSERT INTO `php_role` VALUES ('3', '站点编辑', '', '1', '1', '0', '1', '1');
INSERT INTO `php_role` VALUES ('4', '游客', '', '0', '1', '100', '0', '1');
INSERT INTO `php_role` VALUES ('5', '普通会员', '', '0', '0', '100', '0', '1');

-- ----------------------------
-- Table structure for php_tag
-- ----------------------------
DROP TABLE IF EXISTS `php_tag`;
CREATE TABLE `php_tag` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(30) DEFAULT '' COMMENT 'tag字符',
  `total` mediumint(9) DEFAULT '1' COMMENT '次数',
  PRIMARY KEY (`tid`),
  UNIQUE KEY `name` (`tag`),
  KEY `total` (`total`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Tag标签表';

-- ----------------------------
-- Records of php_tag
-- ----------------------------

-- ----------------------------
-- Table structure for php_upload
-- ----------------------------
DROP TABLE IF EXISTS `php_upload`;
CREATE TABLE `php_upload` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户uid',
  `name` varchar(255) DEFAULT '' COMMENT '原文件名',
  `filename` varchar(100) NOT NULL DEFAULT '' COMMENT '文件名',
  `basename` varchar(100) NOT NULL DEFAULT '' COMMENT '有扩展名的文件名',
  `path` char(200) NOT NULL DEFAULT '' COMMENT '文件路径 ',
  `ext` varchar(45) NOT NULL DEFAULT '' COMMENT '扩展名',
  `image` tinyint(1) NOT NULL DEFAULT '1' COMMENT '图片',
  `size` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `uptime` int(10) NOT NULL DEFAULT '0' COMMENT '上传时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否使用 1 使用 0 未使用',
  `addtime` int(11) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`id`),
  KEY `basename` (`basename`),
  KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='上传文件';

-- ----------------------------
-- Records of php_upload
-- ----------------------------

-- ----------------------------
-- Table structure for php_user
-- ----------------------------
DROP TABLE IF EXISTS `php_user`;
CREATE TABLE `php_user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '角色id',
  `username` char(30) NOT NULL DEFAULT '',
  `nickname` char(30) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` char(40) NOT NULL DEFAULT '',
  `code` char(30) NOT NULL DEFAULT '' COMMENT '密码key',
  `user_status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1  正常  0 锁定',
  `lock_time` char(20) NOT NULL COMMENT '锁定到期时间',
  `qq` char(20) NOT NULL DEFAULT '' COMMENT 'qq号码',
  `email` char(30) NOT NULL DEFAULT '' COMMENT '邮箱',
  `moble` bigint(20) DEFAULT NULL COMMENT '手机号码',
  `sex` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 男 2 女 3 保密',
  `credits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '积分',
  `sign` varchar(255) NOT NULL DEFAULT '' COMMENT '个性签名',
  `spec_num` mediumint(9) unsigned NOT NULL DEFAULT '0' COMMENT '空间访问数',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `regip` char(20) NOT NULL DEFAULT '' COMMENT '注册IP',
  `lastip` char(15) NOT NULL DEFAULT '' COMMENT '最后登录ip',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `regtime` int(11) NOT NULL DEFAULT '0' COMMENT '注册时间',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='会员表';

-- ----------------------------
-- Records of php_user
-- ----------------------------
INSERT INTO `php_user` VALUES ('3', '5', 'chuyou100235', '55', '21232f297a57a5a743894a0e4a801fc3', '', '1', '0', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1431793006', '1430322104');
INSERT INTO `php_user` VALUES ('4', '5', 'cyu10023', 'cyu10023', '53ab5f415825c5368603caa68d35e1e3', '', '1', '0', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430324166', '1430324166');
INSERT INTO `php_user` VALUES ('6', '5', 'fdsfds', 'fdsf', '53ab5f415825c5368603caa68d35e1e3', '', '1', '0', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430324747', '1430324747');
INSERT INTO `php_user` VALUES ('7', '5', 'test', 'test', 'd41d8cd98f00b204e9800998ecf8427e', '', '1', '0', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430324879', '1430324879');
INSERT INTO `php_user` VALUES ('8', '5', 'test123', '123456', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '0', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430325636', '1430325636');
INSERT INTO `php_user` VALUES ('9', '5', '12345', '123456', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '0', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430325677', '1430325677');
INSERT INTO `php_user` VALUES ('10', '5', '666', '66', 'e9510081ac30ffa83f10b68cde1cac07', '', '1', '2015', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430325745', '1430325745');
INSERT INTO `php_user` VALUES ('11', '5', '6666', '66', '21232f297a57a5a743894a0e4a801fc3', '', '1', '2015-04-30 12:43:30', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430325818', '1430325818');
INSERT INTO `php_user` VALUES ('12', '5', 'aaaa', '', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015/04/30 12:52:22', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430326349', '1430326349');
INSERT INTO `php_user` VALUES ('13', '3', 'tianfeng', 'tianfeng', '53ab5f415825c5368603caa68d35e1e3', '', '1', '', '', '', null, '1', '100', '', '0', '', '0.0.0.0', '0.0.0.0', '0', '1430339344');
INSERT INTO `php_user` VALUES ('14', '3', 'test1', '测试管理员', 'e10adc3949ba59abbe56e057f20f883e', '', '1', '', '', '958416459@qq.com', null, '1', '0', '', '0', '', '0.0.0.0', '0.0.0.0', '1430339550', '1430339550');
INSERT INTO `php_user` VALUES ('15', '5', 'ggfgdfg', 'aa', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-02 08:18:45', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430569132', '1430569132');
INSERT INTO `php_user` VALUES ('16', '5', '9584164599', '958416459', '53ab5f415825c5368603caa68d35e1e3', '', '1', '2015-05-02 08:19:05', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1430569154', '1430569154');
INSERT INTO `php_user` VALUES ('17', '3', 'cyu100', '战神', '53ab5f415825c5368603caa68d35e1e3', '', '1', '', '', '1113581489@qq.com', null, '1', '0', '', '0', '', '0.0.0.0', '0.0.0.0', '1430675489', '1430675489');
INSERT INTO `php_user` VALUES ('18', '1', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '', '1', '', '', '', null, '1', '0', '', '0', '', '0.0.0.0', '0.0.0.0', '1432118451', '1431793029');
INSERT INTO `php_user` VALUES ('19', '5', 'username', 'username', '21232f297a57a5a743894a0e4a801fc3', '', '1', '2015-05-20 07:00:07', '', '', null, '1', '0', '这个人很懒,什么都没有留下！', '0', '', '0.0.0.0', '0.0.0.0', '1432119616', '1432119616');
